import inspect

import hangman

def check_get_guessed_word_function():
    print("  >> INFO: checking existence of function get_guessed_word() with two arguments")
    if not "get_guessed_word" in dir(hangman):
        print("  >> ERROR: you must define a function named get_guessed_word()")
        return False
    if not callable(hangman.get_guessed_word):
        print("  >> ERROR: you must define a function named get_guessed_word()")
        return False
    count = len(inspect.signature(hangman.get_guessed_word).parameters)
    if count != 2:
        print("  >> ERROR: your function named get_guessed_word() must take 2 arguments, not", count)
        return False
    return True

def check_get_guessed_word_results():
    print("  >> INFO: checking get_guessed_word('', '')")
    if hangman.get_guessed_word('', '') != '':
        print("  >> ERROR: the result of get_guessed_word('', '') is incorrect")
        return False
    print("  >> INFO: checking get_guessed_word('', 'c')")
    if hangman.get_guessed_word('', 'c') != '':
        print("  >> ERROR: the result of get_guessed_word('', 'c') is incorrect")
        return False
    print("  >> INFO: checking get_guessed_word('centralesupelec', '')")
    if hangman.get_guessed_word('centralesupelec', '') != '***************':
        print("  >> ERROR: the result of get_guessed_word('centralesupelec', '') is incorrect")
        return False
    print("  >> INFO: checking get_guessed_word('centralesupelec', 'c')")
    if hangman.get_guessed_word('centralesupelec', 'c') != 'c*************c':
        print("  >> ERROR: the result of get_guessed_word('centralesupelec', 'c') is incorrect")
        return False
    print("  >> INFO: checking get_guessed_word('centralesupelec', 'cs')")
    if hangman.get_guessed_word('centralesupelec', 'cs') != 'c*******s*****c':
        print("  >> ERROR: the result of get_guessed_word('centralesupelec', 'cs') is incorrect")
        return False
    print("  >> INFO: checking get_guessed_word('centralesupelec', 'centralsup')")
    if hangman.get_guessed_word('centralesupelec', 'centralsup') != 'centralesupelec':
        print("  >> ERROR: the result of get_guessed_word('centralesupelec', 'centralsup') is incorrect")
        return False
    print("  >> INFO: checking get_guessed_word('centralesupelec', 'supcentral')")
    if hangman.get_guessed_word('centralesupelec', 'supcentral') != 'centralesupelec':
        print("  >> ERROR: the result of get_guessed_word('centralesupelec', 'supcentral') is incorrect")
        return False
    print("  >> SUCCESS: your function get_guessed_word() seems to give right results")
    return True


if __name__ == "__main__":
    if not check_get_guessed_word_function():
        exit(-1)
    if not check_get_guessed_word_results():
        exit(-1)
    exit(0)

