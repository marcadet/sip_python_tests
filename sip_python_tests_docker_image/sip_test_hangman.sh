#!/usr/bin/env bash

i=1
while [[ $i -le 10 ]]
do

    echo "  >> INFO: checking existence of file hangman.py"
    if [[ ! -e hangman.py ]]; then
        echo "  >> ERROR: your python script must be called hangman.py"
        exit -1
    fi

    echo "  >> INFO: checking syntax of file hangman.py"
    python -m py_compile hangman.py
    if [[ $? -ne 0 ]]; then
        echo "  >> ERROR: your file hangman.py is not a valid python script"
        exit -1
    fi

    cp /sip/sip_test_hangman.py .
    python sip_test_hangman.py
    if [[ $? -ne 0 ]]; then
        exit -1
    fi

    i=$[$i+1]
done

exit 0

